export default {
    environments: ['gjs', 'node'],
    modules: ['Gtk-4.0', 'Adw-1.0'],
    girDirectories: ['./vala-girs/gir-1.0'],
    ignore: [],
    ignoreVersionConflicts: true,
}
